This project was the mocked project of developing the Speech Therapy dashboard system, a frontend web application system.

# Milestones

## Homepage

Render fluid card grid and display:
* Charts
* An information box
* Tables (displaying data about Students and Active Courses)

Note: Need to understand how to use RavenDB in Node.js, then implement the function fetch data into table.

Deadline: 17/5

## Review skills
Review HTTP methods, Javascript and React. Study simple skeleton of Bootstrap framework.

Deadline: 9/6

# Build and Deploy instructions
## Prerequisites

In order to run this project, you need [the npm program](https://www.npmjs.com/) and [Node.js](https://nodejs.org/) installed in your computer.

Additional dependencies:
* [react-google-charts](https://react-google-charts.com): To display various kind of charts.

## Running

In the project directory, you can run:
```
npm start
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Testing

```
npm test
```

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Building

```
npm run build
```

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment
