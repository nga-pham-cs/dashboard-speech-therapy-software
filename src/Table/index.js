import React from "react";
import "../bootstrap.min.css";
import listActiveCourse from "../Database/listActiveCourse.json";
import listStudents from "../Database/listStudents.json";
import "../ie10-viewport-bug-workaround.css";
import "./index.css";

// For Table styling
const mediumColumn = {
  width: "30%",
};

const smallColumn = {
  width: "10%",
};

export const TableActiveCourses = () => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Title</th>
          <th>Created</th>
          <th># Students</th>
          <th>
            # <input type="checkbox" value="passed" />
          </th>
        </tr>
      </thead>
      <tbody>
        {listActiveCourse.map((item) => {
          return (
            <tr key={item.studentID} className="table-active-row">
              <td>{item.title}</td>
              <td>{item.createdDate}</td>
              <td>{item.studentID}</td>
              <td>{item.passed}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export const TableStudents = () => {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Progress</th>
          <th>Avg. Score</th>
          <th>Completion %</th>
          <th>Exercises Completed this week</th>
          <th>Total XP earned</th>
          <th>Last lesson</th>
          <th>Last seen</th>
          <th>Current course</th>
        </tr>
      </thead>
      <tbody>
        {listStudents.map((item) => {
          return (
            <tr>
              <td className="table-student-row">{item.name}</td>
              <td className="table-student-row">{item.completion}</td>
              <td className="table-student-row">{item.avgScore}</td>
              <td className="table-student-row">{item.completion}</td>
              <td className="table-student-row">{item.exercisedCompleted}</td>
              <td className="table-student-row">{item.totalXP}</td>
              <td className="table-student-row">{item.lastLesson}</td>
              <td className="table-student-row">{item.lastSeen}</td>
              <td className="table-student-row">{item.currentCourse}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
