import React, { Component } from "react";
import "./index.css";
import { TableActiveCourses, TableStudents } from "../Table";
import { BarChart, LineChart } from "../Chart";
import "../bootstrap.min.css";
import "../ie10-viewport-bug-workaround.css";

export class Main_section extends Component {
  render() {
    return (
      <div className="container">
        <section className="info-container row">
          <article className="col-md-3">
            <label>Name</label>
            <main className="large-text">Dr.Victor Frankenstein</main>
          </article>
          <article className="col-md-3">
            <label>Mail</label>
            <main className="medium-text">victor.frankenstein@medel.com</main>
          </article>
          <article className="col-md-3">
            <label>Role</label>
            <main className="medium-text">Rehabilitationist</main>
          </article>
          <article className="col-md-3">
            <label className="bolder">
              To change contact data or role, please request{" "}
              <a href="www.medel.com">here</a>
            </label>
          </article>
        </section>
        <section className="card-container row">
          <article className="col-md-6">
            <header>
              <h4>Bar Chart</h4>
            </header>
            <BarChart />
          </article>

          <article className="col-md-6">
            <header>
              <h4>Line Chart</h4>
            </header>
            <LineChart />
          </article>

          <article className="col-md-12">
            <header className="card__title">
              <h4>Neuigkeiten</h4>
            </header>
            <main className="card__description">
              Lorem Ipsum dolor amet sun Lorem Ipsum dolor amet sun Lorem Ipsum
              dolor amet sun
            </main>
          </article>

          <article className="col-md-12">
            <header className="card__title">
              <h4>Students</h4>
            </header>
            <TableStudents />
          </article>

          <article className="col-md-12">
            <header className="card__title">
              <h4>Active Courses</h4>
            </header>
            <TableActiveCourses />
          </article>
        </section>
      </div>
    );
  }
}
