import React from 'react';
import './App.css';
import {
  Main_section
} from '../homepage';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>Speech Therapy Software dashboard</p>
      </header>
      <body>
        <Main_section/>
      </body>
    </div>
  );
}

export default App;
