import React, {Component} from 'react';
import Chart from 'react-google-charts';

class BarChart extends Component {
    render() {
        return(
            <div style={{marginTop: '5px'}}>
                <Chart
                    width={400}
                    height={230}
                    chartType="ColumnChart"
                    loader={<div>Loading Chart</div>}
                    data={[
                        ['City', '2010 Population'],
                        ['Vienna, VI', 3175000],
                        ['Los Angeles, CA', 3792000],
                        ['London, LD', 5695000],
                        ['Paris, PR', 2099000],
                        ['Rome, RM', 4526000],
                    ]}
                    options={{
                        title: 'Population of Largest EU Cities',
                        chartArea: { width: '100%' },
                        hAxis: {
                            title: 'Total Population',
                            minValue: 0,
                        },
                        vAxis: {
                            title: 'City',
                        },
                    }}
                    legendToggle
                />
            </div>
        )
    }
}

class LineChart extends Component {
    render() {
        return(
            <div style={{marginTop: '5px'}}>
                <Chart
                    width={500}
                    height={200}
                    chartType="Line"
                    loader={<div>Loading Chart</div>}
                    data={[
                        [
                            'Movies',
                            'Dr. Strange',
                            'The Avengers',
                            'Black Widow',
                        ],
                        [1, 37.8, 80.8, 41.8],
                        [2, 30.9, 69.5, 32.4],
                        [3, 25.4, 57, 25.7],
                        [4, 11.7, 18.8, 10.5],
                        [5, 11.9, 17.6, 10.4],
                        [6, 8.8, 13.6, 7.7],
                        [7, 7.6, 12.3, 9.6],
                        [8, 12.3, 29.2, 10.6],
                        [9, 16.9, 42.9, 14.8],
                        [10, 12.8, 30.9, 11.6],
                        [11, 5.3, 7.9, 4.7],
                        [12, 6.6, 8.4, 5.2],
                        [13, 4.8, 6.3, 3.6],
                        [14, 4.2, 6.2, 3.4],
                    ]}
                    options={{
                        chart: {
                            title: 'Box Office Earnings in First Two Weeks of Opening',
                            subtitle: 'in millions of dollars (USD)',
                        },
                    }}
                />
            </div>
        )
    }
}

export {
    BarChart,
    LineChart,
}