import React from 'react';
import { DocumentStore } from "ravendb";

const store = new DocumentStore(
    ["http://127.0.0.1:8080"],  // URL to the Server
    "ActiveCourses"             // Default database that DocumentStore will interact with
);

//store.initialize();     // required, to establish connection with the Server
//store.dispose();        // Unresolved function